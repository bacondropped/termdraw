termdraw v0.2 to-do list
========================

General
-------
+ Option for printing/not printing filename

Graphs
------
+ Horizontal string-value graphs (ref:
  https://pypi.python.org/pypi/ascii_graph/0.2.1)
+ Graph views
+ Graph view decorations
