termdraw v1.0 to-do list
========================

General
-------
+ Tests
+ Option for printing/not printing filename

Graphs
------
+ Histogram graphs
+ Line graphs (ref: http://www.algorithm.co.il/blogs/ascii-plotter/)
+ Heat maps for large quantity of points, implement with ░▒▓█ symbols for
  Unicode
+ Inline point graph value labels
+ Inline line graph value labels
+ Non-binary interpolation support for `_interpolate_points`
+ Cubic interpolation
+ Cosine interpolation
